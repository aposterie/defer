"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Deferred {
    constructor() {
        this.isSettled = Status.Pending;
        const promise = new Promise((resolve, reject) => {
            this.resolve = async (raw) => {
                const value = await raw;
                this.isSettled = Status.Resolved;
                this.value = value;
                resolve(value);
            };
            this.reject = (reason) => {
                this.isSettled = Status.Rejected;
                reject(reason);
            };
        });
        this.then = promise.then.bind(promise);
        this.catch = promise.catch.bind(promise);
        this.finally = promise.finally.bind(promise);
    }
}
exports.Deferred = Deferred;
var Status;
(function (Status) {
    Status["Pending"] = "Pending";
    Status["Resolved"] = "Resolved";
    Status["Rejected"] = "Rejected";
})(Status = exports.Status || (exports.Status = {}));
