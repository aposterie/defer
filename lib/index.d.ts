declare type MaybePromise<T> = T | PromiseLike<T>;
export interface Deferred<T> extends Promise<T> {
    [Symbol.toStringTag]: "Promise";
    resolve: (value?: MaybePromise<T>) => void;
    reject: (reason?: any) => void;
}
export declare class Deferred<T = void> implements Promise<T> {
    isSettled: Status;
    value?: T;
    constructor();
}
export declare enum Status {
    Pending = "Pending",
    Resolved = "Resolved",
    Rejected = "Rejected"
}
export {};
